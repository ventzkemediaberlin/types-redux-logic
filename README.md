TypeScript definitions for `redux-logic`
==

This project contains the TypeScript type definitions for the [`redux-logic`](https://github.com/jeffbski/redux-logic) project.

The version number of this project should track the version number of `redux-logic` as closely as possible.

